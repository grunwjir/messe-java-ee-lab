--
-- JBoss, Home of Professional Open Source
-- Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
-- contributors by the @authors tag. See the copyright.txt in the
-- distribution for a full listing of individual contributors.
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
-- http://www.apache.org/licenses/LICENSE-2.0
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

-- PRODUCTION DATA
insert into products (id, name, price_per_km, base_price) values (0, 'Standard', 10, 30);
insert into products (id, name, price_per_km, base_price) values (1, 'Express', 12, 60);
insert into products (id, name, price_per_km, base_price) values (2, 'Extreme', 15, 150);

-- DEV DATA -- password=pas
INSERT INTO public.member (id, email, fee, name, customer_number, password, phone_number) VALUES (1, 'albert@gmail.com', null, 'Albert', null, 'fx8HPYu5suOtbIhQGWYNG1GzifjpFp69FVhkZ/i8h+Y=' ,'48974');
INSERT INTO public.member (id, email, fee, name, customer_number, password, phone_number) VALUES (2, 'lionel@gmail.com', 0.15, 'Lionel', null, 'fx8HPYu5suOtbIhQGWYNG1GzifjpFp69FVhkZ/i8h+Y=' ,'978798');
INSERT INTO public.member (id, email, fee, name, customer_number, password, phone_number) VALUES (3, 'jonas@gmail.com', null, 'Jonas', null, 'fx8HPYu5suOtbIhQGWYNG1GzifjpFp69FVhkZ/i8h+Y=' ,'4846');
INSERT INTO public.member (id, email, fee, name, customer_number, password, phone_number) VALUES (4, 'james@gmail.com', null, 'James', null, 'fx8HPYu5suOtbIhQGWYNG1GzifjpFp69FVhkZ/i8h+Y=' ,'4894987');

INSERT INTO public.user_roles (user_role_id, role, member_id) VALUES (1, 'CUSTOMER', 1);
INSERT INTO public.user_roles (user_role_id, role, member_id) VALUES (2, 'COURIER', 2);
INSERT INTO public.user_roles (user_role_id, role, member_id) VALUES (3, 'OPERATOR', 3);
INSERT INTO public.user_roles (user_role_id, role, member_id) VALUES (4, 'ADMIN', 4);

INSERT INTO public.consignment_points (id, city, house_number, postal_code, street, date_from, date_to, name, note, telephone) VALUES (1, 'Praha', '9', '160 00', 'Thákurova', '2016-06-16 21:06:26.625000', '2016-06-16 21:06:26.625000', 'Milan Zelený', null, '420 779 897');
INSERT INTO public.consignment_points (id, city, house_number, postal_code, street, date_from, date_to, name, note, telephone) VALUES (2, 'Praha', '8', '140 00', 'Na lepším', '2016-06-16 21:06:26.625000', '2016-06-16 21:06:26.625000', 'Jan Novák', null, '795 878 266');
INSERT INTO public.consignment_points (id, city, house_number, postal_code, street, date_from, date_to, name, note, telephone) VALUES (10, 'Praha', '9', '160 00', 'Thákurova', '2016-06-17 00:12:27.815000', '2016-06-17 00:12:27.815000', 'Milan Zelený', null, '420 779 897');
INSERT INTO public.consignment_points (id, city, house_number, postal_code, street, date_from, date_to, name, note, telephone) VALUES (11, 'Praha', '8', '140 00', 'Na lepším', '2016-06-17 00:12:27.815000', '2016-06-17 00:12:27.815000', 'Jan Novák', null, '795 878 266');

INSERT INTO public.consignments (id, code, is_accounted, price, state, courier_id, customer_id, delivery_point_id, pickup_point_id, product_id) VALUES (3, '1466103986625', FALSE , 204.37, 'ASSIGNED', 2, 1, 1, 2, 0);
INSERT INTO public.consignments (id, code, is_accounted, price, state, courier_id, customer_id, delivery_point_id, pickup_point_id, product_id) VALUES (12, '1466115147815', FALSE , 204.37, 'DELIVERED', 2, 1, 10, 11, 0);

INSERT INTO public.packages (id, depth, height, weight, width, consignment_id) VALUES (4, 50, 50, 1000, 50, 3);
INSERT INTO public.packages (id, depth, height, weight, width, consignment_id) VALUES (13, 50, 50, 1000, 50, 12);


-- reset all sequences
ALTER SEQUENCE hibernate_sequence RESTART WITH 20;
