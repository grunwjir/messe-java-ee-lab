(function() {
	'use strict';
	angular.module('messe',
		[
			'ui.router',
			'ui.bootstrap',
			'toaster', 'ngAnimate',

			'messe.util',
			'messe.consignment',
			'messe.operator',
			'messe.courier',
			'messe.user',
			'messe.navbar',
			'messe.websocket'
			
		])
		.constant('BASE_URL', 'resources/app/')
		.constant('API_REST', 'rest')
		.constant('WS_URL', 'wss://localhost:8443/Messe/deliveries')
		.config(function ($stateProvider, $urlRouterProvider, $locationProvider, BASE_URL) {
			$stateProvider
			.state('app', {
				url : '/',
				abstract: true,
				templateUrl: BASE_URL + 'app.html',
				controller: 'MainController',
				resolve: {
					user : function(userService){
						return userService.getMe();
					}
				}

				});

			$urlRouterProvider.otherwise("userprofile/");

	})
	.controller('MainController', function MainController(user, storeService){
		storeService.put('me',user.data); 
	});
})();
