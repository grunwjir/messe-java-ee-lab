(function() {

  var ListUserController = function(users, $uibModal, userService) {
    var vm = this;
	this.users = users.data;
    
	this.deleteUser = function(id){
		userService.deleteUser(id).then(function () {
			userService.getList().then(function(response){
	    		vm.users = response.data;
			});
		});  
	};
	
    this.newUser = function(){
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'resources/app/user/modal/user-form-modal.html',
            controller: 'UserFormModal',
            controllerAs: 'vm',
            size: 'md',
            resolve: {
            	user: function(){
            		return {
            			data : {
            				roles : []
            			}
            		};
            	},
            	modal: function(){
            		return {
            			title : 'New user',
            			button : 'Create'
            		}
            	},
            	roles: function(userService){
            		return userService.getRolesList();
            	}
            }
          });

          modalInstance.result.then(function () {
        	  userService.getList().then(function(response){
        		  vm.users = response.data;
        	  });
          }, function () {
        	  //dismissed
          });
    };
    
    this.editUser = function(id){
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'resources/app/user/modal/user-form-modal.html',
            controller: 'UserFormModal',
            controllerAs: 'vm',
            size: 'md',
            resolve: {
            	user: function(userService){
            		return userService.getById(id);
            	},
            	modal: function(){
            		return {
            			title : 'Edit user',
            			button : 'Update'
            		}
            	},
            	roles: function(userService){
            		return userService.getRolesList();
            	}
            }
          });

          modalInstance.result.then(function () {
        	  userService.getList().then(function(response){
        		  vm.users = response.data;
        	  });
          }, function () {
        	  //dismissed
          });
    };
    

  };

  angular.module('messe.user.list',
    [
     	"checklist-model",
     	"messe.user.list.modal"
    ])

    .controller('ListUserController', ListUserController);

})();