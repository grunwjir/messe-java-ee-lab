(function() {

  var UserFormModal = function($uibModalInstance, user, modal, roles, userService, toaster) {
    
	  this.modal = modal;
	  this.user = user.data;
	  this.roles = roles.data;
	  
	  this.cancel = function(){
		  $uibModalInstance.dismiss('cancel'); 
	  };
	  
	  this.ok = function(){
		  if(this.user && this.user.id != undefined){
			  // edit
			  userService.update(this.user).then(function(response){
				  $uibModalInstance.close();
			  }, function errorCallback(response) {
				  angular.forEach(response.data, function(value, key) {
					  toaster.pop('error', key, value);
				  });
			  });
		  }else{
			  // create
			  userService.save(this.user).then(function(response){
				  $uibModalInstance.close();
			  }, function errorCallback(response) {
				  angular.forEach(response.data, function(value, key) {
					  toaster.pop('error', key, value);
				  });
			  });
		  }
	  };
  };
	
  angular.module('messe.user.list.modal', [
	  
  ])
    .controller('UserFormModal', UserFormModal);

})();