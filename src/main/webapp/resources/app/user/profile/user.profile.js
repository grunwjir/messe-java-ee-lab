(function() {

    var UserProfileController = function(storeService, turnoverService) {
        var vm = this;
        vm.turnovers = [];
        vm.user = storeService.get('me');
        
        vm.isCourier = function () {
            return vm.user.roles.indexOf('courier'.toUpperCase()) >= 0;
        };
        
        vm.showTurnovers = function () {
            return vm.turnovers && vm.turnovers.length > 0;  
        };

        var getTurnovers = function(){
            var user = storeService.get('me');
            var idx = user.roles.indexOf('courier'.toUpperCase());
            if (idx >= 0) {
                turnoverService.getMyList().then(function (turnovers) {
                    vm.turnovers = turnovers.data;
                });
            }
        };
        
        getTurnovers();

    };

    angular.module('messe.user.profile',
        [
        ])

        .controller('UserProfileController', UserProfileController);

})();