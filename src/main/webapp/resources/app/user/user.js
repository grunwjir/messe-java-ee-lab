(function() {
	'use strict';
	angular.module('messe.user',
		[
			'ui.router',
			
			'messe.user.list',
			'messe.user.profile'
			
		])
		.constant('BASE_USER_URL', 'resources/app/user/')
		.config(function ($stateProvider, $urlRouterProvider, BASE_USER_URL) {

			$stateProvider
				.state('app.user', {
					url : 'users/',
					templateUrl: BASE_USER_URL + 'list/list-user.html',
					controller: 'ListUserController',
			        controllerAs: 'vm',
					resolve : {
						users: function(userService){
				    		return userService.getList();
				    	}
					}			
				})
				.state('app.userprofile', {
					url : 'userprofile/',
					templateUrl: BASE_USER_URL + 'profile/user-profile.html',
					controller: 'UserProfileController',
					controllerAs: 'vm'
			});
		})
	
})();
