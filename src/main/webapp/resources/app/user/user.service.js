(function() {

  var UserService = function($http, API_REST) {
    
	this.getCouriersList = function() {
      return $http.get(API_REST + '/users/couriers');
    };
    
	this.getCustomersList = function() {
	      return $http.get(API_REST + '/users/customers');
	};

	this.getRolesList = function() {
	      return $http.get(API_REST + '/users/roles');
	};
	
	this.getList = function() {
	      return $http.get(API_REST + '/users');
	};

	this.save = function(user){
		return $http.post(API_REST + '/users', user);
	}; 
	
	this.update = function(user){
		return $http.put(API_REST + '/users/' + user.id, user);
	}; 
	
	this.getById = function(id){
		return $http.get(API_REST + '/users/' + id);
	};
	
	this.deleteUser = function(id){
		return $http.delete(API_REST + '/users/' + id);
	};
	  
	this.getMe = function () {
		return $http.get(API_REST + '/users/me');
	}  
    
  };  
 
  angular.module('messe.user')
    .service('userService', UserService);
})();
