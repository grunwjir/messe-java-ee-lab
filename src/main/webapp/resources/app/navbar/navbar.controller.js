(function() {
    "use strict";

    var NavigationController = function(storeService, $scope, $timeout) {
        var nav = this;
        nav.user = storeService.get('me') || null;

        /**
         * Pro roleName vrati, jestli povoleno nebo ne. Admin vidi vse.
         * @param roleName role nebo prazdny (tj. netreba vsude psat admin roli)
         * @returns {boolean} zobraz polozku nebo ne
         */
        nav.showFor = function(roleName) {
            if (roleName === 'all' || nav.isAdmin()) {
                return true;
            }
            
            if (!roleName) {
                return false;
            }
            
            var idx = nav.user.roles.indexOf(roleName.toUpperCase());
            return idx > -1;
        };
        
        nav.isAdmin = function () {
            var idx = nav.user.roles.indexOf('ADMIN');
            return idx > -1;
        };

    };

    angular.module('messe.navbar',
        [

        ])

        .controller('NavigationController', NavigationController);

})();

