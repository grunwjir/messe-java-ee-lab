(function() {
    'use strict';
    
    angular.module('messe.websocket', []) 
        .service('deliveryService', function (WS_URL, orderService) {

            var self = this;
            self.listeners = [];

            self.onMessage = function(event) {
                var delivery = JSON.parse(event.data);

                if (delivery.action === "update") {
                    self.notifyAll();
                }
            };

            self.sendUpdate = function() {
                var action = {
                    action: "update"
                };
                self.socket.send(JSON.stringify(action));
            };
            
            self.registerCallback = function (callback) {
                self.listeners.push(callback);
            };
            
            self.notifyAll = function () {
                for (var i = 0; i < self.listeners.length; i++) {
                    var listener = self.listeners[i];
                    listener();
                }
            };

            self.socket = new WebSocket(WS_URL);
            self.socket.onmessage = self.onMessage;

    });

})();