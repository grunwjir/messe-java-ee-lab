(function() {

    var StoreService = function() {
        
        var service = this;
        service.hashmap = [];

        service.put = function (key, val) {
            service.hashmap[key] = val;
        };

        service.get = function (key) {
            return service.hashmap[key];
        };

        service.remove = function (key) {
            delete service.hashmap[key];
        };
    };

    angular.module('messe.util', []);

    angular.module('messe.util')
        .service('storeService', StoreService);
})();
