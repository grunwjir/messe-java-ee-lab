(function() {

  var TurnoverService = function($http, API_REST) {
    this.getList = function() {
      return $http.get(API_REST + '/turnovers');
    };

    this.getMyList = function() {
      return $http.get(API_REST + '/turnovers/my');
    };

    this.generate = function (fromDate, toDate) {
      var params = {"fromDate": fromDate, "toDate": toDate};
      return $http({
        url: API_REST+'/turnovers/generate',
        method: 'POST',
        params: params
      })
    };
    
  };  
    
  angular.module('messe.courier')
    .service('turnoverService', TurnoverService);
})();
