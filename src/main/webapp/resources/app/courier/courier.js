(function() {
	'use strict';
	angular.module('messe.courier',
		[
			'ui.router',
			
			'messe.courier.list.turnovers',
			'messe.courier.list.orders'
		])
		.constant('BASE_COURIER_URL', 'resources/app/courier/')
		.config(function ($stateProvider, $urlRouterProvider, BASE_COURIER_URL) {

			$stateProvider
				.state('app.courier-turnovers', {
					url : 'couriers-turnovers/',
					templateUrl: BASE_COURIER_URL + 'list/list-courier-turnover.html',
					controller: 'ListCourierTurnoverController',
			        controllerAs: 'vm',
					resolve : {
						turnovers: function(turnoverService){
				    		return turnoverService.getList();
				    	}
					}			
				})
				.state('app.courier-orders', {
					url : 'courier-orders/',
					templateUrl: BASE_COURIER_URL + 'list/list-courier-order.html',
					controller: 'ListCourierOrderController',
					controllerAs: 'vm',
					resolve : {
						orders: function(orderService){
							return orderService.getCourierOrdersList();
						}
					}
				});
		})
	
})();
