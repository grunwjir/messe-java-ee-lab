(function() {

  var ListCourierOrderController = function(orders, orderService, deliveryService, $timeout, $scope) {
	var vm = this; 
	vm.orders = orders.data;
      
    vm.startDelivering = function (orderId) {
        orderService.startDelivering(orderId);
    };
      
    vm.wasDelivered = function (orderId) {
        orderService.wasDelivered(orderId);
    };
      
    vm.isDelivering = function (orderState) {
        return orderState.toUpperCase() === 'delivering'.toUpperCase();
    };
      
    vm.onUpdate = function () {
        orderService.getCourierOrdersList().then(vm.processUpdate);
    };

    vm.processUpdate = function (orders) {
        console.log('processUpdate');
        // timeout zajisti, ze $apply probehne pri dalsim $digest a nevznikne tak konflikt 
        $timeout(function () {
            vm.orders = orders.data;
            $scope.$apply();
        });
    };
    
    deliveryService.registerCallback(vm.onUpdate);
      
  };

  angular.module('messe.courier.list.orders',
    [
  
    ])

    .controller('ListCourierOrderController', ListCourierOrderController);

})();