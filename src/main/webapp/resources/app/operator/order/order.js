(function() {
	'use strict';
	angular.module('messe.operator.order',
		[
			'ui.router',
			'messe.operator.order.create'
		])
		.constant('BASE_OPERATOR_ORDER_URL', 'resources/app/operator/order/')
		.config(function ($stateProvider, $urlRouterProvider, BASE_OPERATOR_ORDER_URL) {

			$stateProvider
				.state('app.operator.order-create', {
					url : 'order/create',
					templateUrl: BASE_OPERATOR_ORDER_URL + 'create/create-order.html',
					controller: 'CreateOrderController',
				    controllerAs: 'vm',
				    resolve: {
				    	products: function(productService){
				    		return productService.getList();
				    	},
				    	customers: function(userService){
				    		return userService.getCustomersList();
				    	},
				    	couriers: function(userService){
				    		return userService.getCouriersList();
				    	}
				    }
				    	
				});
		})
	
})();

