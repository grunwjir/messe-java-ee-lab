(function() {

    var ProductService = function($http, API_REST) {
    this.getList = function() {
      return $http.get(API_REST + '/products');
    };
  };  
    
  var OrderService = function($http, API_REST) {
    this.save = function(order) {
    	return $http.post(API_REST + '/orders', order);	
    };
    
    this.getList = function() {
        return $http.get(API_REST + '/orders');
    };
    
    this.getPrice = function(order){
    	return $http.get(API_REST + '/orders/computePrice', {
    	    params: { 
    	    	productId : order.product,
    	    	origins : order.pickup.street + " " + order.pickup.houseNumber + " " + order.pickup.city,
    	    	destinations : order.delivery.street + " " + order.delivery.houseNumber + " " +order.delivery.city
    	    }
    	});
    };

    this.getCourierOrdersList = function() {
        return $http.get(API_REST + '/orders/mine/undelivered/');
    };

    this.startDelivering = function(orderId) {
      return $http.post(API_REST + '/orders/' + orderId + '/startDelivering');
    };

    this.wasDelivered = function(orderId) {
      return $http.post(API_REST + '/orders/' + orderId + '/wasDelivered');
    };  
      
  };  

  angular.module('messe.operator.order')
    .service('productService', ProductService)
    .service('orderService', OrderService);
})();
