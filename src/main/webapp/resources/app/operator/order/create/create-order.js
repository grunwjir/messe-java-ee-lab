(function() {

  var CreateOrderController = function(products, couriers, customers, orderService, $state, toaster) {

	this.products = products.data;
	this.customers = customers.data;
	this.couriers = couriers.data;
	  
    this.order = {
    	id : null,
    	code : new Date().getTime(),
    	product : this.products[0].id,
    	customer : this.customers[0].id,
    	courier : this.couriers[0].id,
    	price : 0.0,
    	pickup : {
    		  name : "Jan Novák",
    		  telephone : "795 878 266",
    		  street : "Na lepším",
    		  houseNumber : "8",
    		  postalcode : "140 00",
    		  city : "Praha",
			  dateFrom : new Date(),
			  dateTo : new Date()
    	},
    	delivery : {
    		  name : "Milan Zelený",
    		  telephone : "420 779 897",
    		  street : "Thákurova",
    		  houseNumber : "9",
    		  postalcode : "160 00",
    		  city : "Praha",
			  dateFrom : new Date(),
			  dateTo : new Date()
    	},
    	packages : [{
    		weight : 1000,
    		height : 50,
    		width : 50,
    		depth : 50
    	}]
    };
    
    this.save = function() {
    	console.log("save order");
    	console.log(this.order);
		
        orderService.save(this.order).then(function() {
			$state.go('app.consignment');
        }, function errorCallback(response) {
        	toaster.pop('error', "Error", "Order wasn´t created.");  
		  });
      
    };
    
  };

  angular.module('messe.operator.order.create',
    [
     	'messe.operator.order.form'
    ])
    .controller('CreateOrderController', CreateOrderController);

})();
