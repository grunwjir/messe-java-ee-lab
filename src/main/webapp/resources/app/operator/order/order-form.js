(function() {
  var orderFormController = function(orderService) {
  
    this.save = function(form) {
    	this.onSave({order: this.order});
    };
    
    this.addPackage = function(){
    	this.order.packages.push({
    		weight : 1000,
    		height : 50,
    		width : 50,
    		depth : 50
    	});
    };
    
    this.priceok = false;
    
    this.removePackage = function(){
    	if(this.order.packages.length > 1){
    		this.order.packages.pop();
    	}
    };
    
    this.updatePrice = function(){
    	var vm = this;
    	orderService.getPrice(this.order).then(function(response) {
    		if(response.status == 200){
    			vm.priceok = true;
    			vm.order.price = response.data;
    		}else{
    			vm.priceok = false;
    			vm.order.price = 0.0;
    		}
        });
    };
    
  };

  angular.module('messe.operator.order.form',
    [
    ])
    .component('orderForm', {
      bindings: {
    	  order: '=',
          onSave: '&',
          products: '=',
          customers: '=',
          couriers: '='
      },

      controller: orderFormController,
      controllerAs: 'vm',
      templateUrl: 'resources/app/operator/order/order-form.html'
    });
})();
