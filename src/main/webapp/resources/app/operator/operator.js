(function() {
	'use strict';
	angular.module('messe.operator',
		[
			'ui.router',
			
			'messe.operator.order'
		])
		.constant('BASE_OPERATOR_URL', 'resources/app/operator/')
		.config(function ($stateProvider, $urlRouterProvider, BASE_OPERATOR_URL) {

			$stateProvider
				.state('app.operator', {
					url : 'operators/',
					templateUrl: BASE_OPERATOR_URL + 'operator.html'				
				});
		})
	
})();
