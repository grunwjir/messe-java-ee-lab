(function() {
	'use strict';
	angular.module('messe.consignment',
		[
			'ui.router',
			
			'messe.consignment.list'
		])
		.constant('BASE_CONSIGNMENT_URL', 'resources/app/consignment/')
		.config(function ($stateProvider, $urlRouterProvider, BASE_CONSIGNMENT_URL) {

			$stateProvider
				.state('app.consignment', {
					url : 'consignments/',
					templateUrl: BASE_CONSIGNMENT_URL + 'list/list-consignment.html',
					controller: 'ListConsignmentController',
			        controllerAs: 'vm',
					resolve : {
						orders: function(orderService){
				    		return orderService.getList();
				    	}
					}
				});
		})
	
})();
