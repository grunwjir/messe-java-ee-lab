(function() {

  var ListConsignmentController = function(orders, deliveryService, $scope, orderService, $timeout) {
    this.orders = orders.data;
    
    this.onUpdate = function () {
      orderService.getList().then(this.processUpdate.bind(this));
    };

    this.processUpdate = function (orders) {
      // timeout zajisti, ze $apply probehne pri dalsim $digest a nevznikne tak konflikt 
      $timeout(function () {
        this.orders = orders.data;
        $scope.$apply();
      }.bind(this));
    };
    
    deliveryService.registerCallback(this.onUpdate.bind(this));
    
  };

  angular.module('messe.consignment.list',
    [
  
    ])

    .controller('ListConsignmentController', ListConsignmentController);

})();