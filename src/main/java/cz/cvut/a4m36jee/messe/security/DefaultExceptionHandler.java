package cz.cvut.a4m36jee.messe.security;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DefaultExceptionHandler implements ExceptionMapper<Exception> {


    @Override
    public Response toResponse(Exception e) {
        if(e instanceof WebApplicationException) {
            WebApplicationException exception = (WebApplicationException) e;
            return exception.getResponse();
        }
        return Response.serverError().build();
    }
}