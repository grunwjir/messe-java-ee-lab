package cz.cvut.a4m36jee.messe.security;

import org.jboss.security.auth.spi.Util;

public class DigestPassword {

    public static String hash(String password) {
        return Util.createPasswordHash("SHA-256", "BASE64", null, null, password);
    }
}