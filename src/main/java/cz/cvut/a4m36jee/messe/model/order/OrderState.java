package cz.cvut.a4m36jee.messe.model.order;

public enum OrderState {
    CREATED, ASSIGNED, DELIVERING, DELIVERED;

    public static OrderState fromString(String s) {
        if (CREATED.name().equalsIgnoreCase(s)) {
            return CREATED;
        }
        if (ASSIGNED.name().equalsIgnoreCase(s)) {
            return ASSIGNED;
        }
        if (DELIVERING.name().equalsIgnoreCase(s)) {
            return DELIVERING;
        }
        if (DELIVERED.name().equalsIgnoreCase(s)) {
            return DELIVERED;
        }
        return null;
    }
}

