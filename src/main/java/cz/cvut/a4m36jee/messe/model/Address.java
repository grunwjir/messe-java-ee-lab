package cz.cvut.a4m36jee.messe.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@SuppressWarnings("serial")
@XmlRootElement
@Embeddable
public class Address implements Serializable{


	
	private String street;
	
	@Column(name="postal_code")
	private String postalCode;

	@Column(name="house_number")
	private String houseNumber;
	
	private String city;

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddressLine1() {
		return getStreet() + " " + getHouseNumber();
	}

	public String getAddressLine2() {
		return getCity() + ", " + getPostalCode();
	}
		
}
