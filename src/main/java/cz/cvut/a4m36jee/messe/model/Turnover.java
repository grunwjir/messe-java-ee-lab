package cz.cvut.a4m36jee.messe.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"courier_id", "from_date", "to_date"})})
@NamedQueries({
        @NamedQuery(name = "Turnover.findAllForCourier", query = "SELECT t FROM Turnover t WHERE t.courier = :courier")
})
public class Turnover {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "from_date")
    private Date fromDate;
    
    @Column(name = "to_date")
    private Date toDate;

    @Column(name = "created")
    private Date created;
    
    @Column(name = "consignmentSize")
    private Integer consignmentSize;
    
    @Column(name = "profit")
    private BigDecimal profit; 
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "courier_id")
    private Member courier;

    @NotNull
    @Column(length=8192)
    private String message;

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public String getMessage() {
        return message;
    }

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Integer getConsignmentSize() {
		return consignmentSize;
	}

	public void setConsignmentSize(Integer consignmentSize) {
		this.consignmentSize = consignmentSize;
	}

	public BigDecimal getProfit() {
		return profit;
	}

	public void setProfit(BigDecimal profit) {
		this.profit = profit;
	}

	public Member getCourier() {
		return courier;
	}

	public void setCourier(Member messenger) {
		this.courier = messenger;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
    
}
