package cz.cvut.a4m36jee.messe.converter;

import cz.cvut.a4m36jee.messe.dto.MemberDTO;
import cz.cvut.a4m36jee.messe.model.Member;
import cz.cvut.a4m36jee.messe.model.Role;
import cz.cvut.a4m36jee.messe.security.DigestPassword;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

public class MemberDtoConverter extends SuperDtoConverter<Member, MemberDTO> {

    public MemberDTO convert(Member m) {
        if (m == null) {
            return null;
        }
        MemberDTO convertToDto = new MemberDTO();
        convertToDto.setId(m.getId().toString());
        convertToDto.setName(m.getName());
        convertToDto.setEmail(m.getEmail());
        convertToDto.setPhoneNumber(m.getPhoneNumber());
        if (m.getFee() != null) {
            convertToDto.setFee(m.getFee().toString());
        }
        Set<String> roles = new HashSet<>();
        for (Role r : m.getRoles()) {
            roles.add(r.getRole());
        }
        convertToDto.setRoles(roles);
        return convertToDto;
    }

    public Member convertDto(MemberDTO memberDTO, Member convertTo) {
        if (memberDTO.getId() != null) {
            convertTo.setId(Long.valueOf(memberDTO.getId()));
        }
        convertTo.setEmail(memberDTO.getEmail());
        if (memberDTO.getPassword() != null) {
            convertTo.setPassword(DigestPassword.hash(memberDTO.getPassword()));
        }
        convertTo.setPhoneNumber(memberDTO.getPhoneNumber());
        convertTo.setName(memberDTO.getName());
        if (memberDTO.getFee() != null) {
            convertTo.setFee(BigDecimal.valueOf(Double.valueOf(memberDTO.getFee())));
        }
        Set<Role> roles = new HashSet<>();
        for (String s : memberDTO.getRoles()) {
            roles.add(new Role(s));
        }
        convertTo.setRoles(roles);
        return convertTo;
    }
}
