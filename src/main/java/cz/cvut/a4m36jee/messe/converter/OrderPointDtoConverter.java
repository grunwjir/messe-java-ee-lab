package cz.cvut.a4m36jee.messe.converter;

import cz.cvut.a4m36jee.messe.dto.order.OrderPointDTO;
import cz.cvut.a4m36jee.messe.model.order.OrderPoint;

public class OrderPointDtoConverter extends SuperDtoConverter<OrderPoint, OrderPointDTO> {
	
	@Override
	public OrderPointDTO convert(OrderPoint o) {	
		if(o == null){
			return null;
		}
		return new OrderPointDTO(
			o.getName(), 
			o.getTelephone(), 
			o.getNote(), 
			o.getAddress().getStreet(),
			o.getAddress().getPostalCode(), 
			o.getAddress().getHouseNumber(),
			o.getAddress().getCity(), 
			o.getDateFrom().toString(),
			o.getDateTo().toString());
	}

	@Override
	public OrderPoint convertDto(OrderPointDTO input) {
		return null;
	}
}
