package cz.cvut.a4m36jee.messe.converter;

import cz.cvut.a4m36jee.messe.dto.order.ProductDTO;
import cz.cvut.a4m36jee.messe.model.product.Product;

public class ProductDtoConverter extends SuperDtoConverter<Product, ProductDTO> {

	@Override
	public ProductDTO convert(Product p) {	
		if(p == null){
			return null;
		}
		return new ProductDTO(p.getId().toString(), p.getName(), p.getPricePerKm().toPlainString(), p.getBasePrice().toPlainString());
	}

	@Override
	public Product convertDto(ProductDTO input) {
		return null;
	}

}
