package cz.cvut.a4m36jee.messe.converter;

import cz.cvut.a4m36jee.messe.data.MemberRepository;
import cz.cvut.a4m36jee.messe.data.product.ProductRepository;
import cz.cvut.a4m36jee.messe.dto.order.OrderDTO;
import cz.cvut.a4m36jee.messe.dto.order.OrderPointDTO;
import cz.cvut.a4m36jee.messe.dto.order.PackageDTO;
import cz.cvut.a4m36jee.messe.model.Address;
import cz.cvut.a4m36jee.messe.model.order.*;
import cz.cvut.a4m36jee.messe.service.PriceService;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class OrderDtoConverter extends SuperDtoConverter<Delivery, OrderDTO> {
	private static final DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");

	@Inject
	OrderPointDtoConverter orderPointDtoConverter;
	@Inject
	PackageDtoConverter packagePointDtoConverter;

	@Inject
	private ProductRepository productRepository;
	@Inject
	private MemberRepository memberRepository;

	@Override
	public OrderDTO convert(Delivery o) {
		if(o == null){
			return null;
		}
		return new OrderDTO(
			o.getId().toString(),
			o.getState().toString(),
			o.getCode(),
			o.getPrice().toPlainString(), 
			o.getProduct().getId().toString(),
			o.getCustomer().getId().toString(),
			o.getCourier().getId().toString(),
			packagePointDtoConverter.convertToList(o.getPackages()), 
			orderPointDtoConverter.convert(o.getPickupPoint()), 
			orderPointDtoConverter.convert(o.getDeliveryPoint()),
			o.getProduct().getName(),
			o.getCourier().getName(),
			o.getCustomer().getName());
	}

	@Override
	public Delivery convertDto(OrderDTO order) throws ParseException {
		Delivery or = new Delivery();

		or.setCode(order.getCode());
		or.setPickupPoint(convertOrderPoint(order.getPickup()));
		or.setDeliveryPoint(convertOrderPoint(order.getDelivery()));

		if (order.getPrice() != null) {
			or.setPrice(BigDecimal.valueOf(Double.valueOf(order.getPrice())));
		}
		or.setState(OrderState.fromString(order.getState()));
		or.setProduct(productRepository.findById(Long.parseLong(order.getProduct())));
		or.setCustomer(memberRepository.findById(Long.parseLong(order.getCustomer())));
		or.setCourier(memberRepository.findById(Long.parseLong(order.getCourier())));
		or.setPackages(convertPackages(order.getPackages()));
		return or;
	}

	private OrderPoint convertOrderPoint(OrderPointDTO orderPoint) throws ParseException {

		OrderPoint op = new OrderPoint();

		op.setDateFrom(format.parse(orderPoint.getDateFrom()));
		op.setDateTo(format.parse(orderPoint.getDateTo()));
		op.setName(orderPoint.getName());
		op.setTelephone(orderPoint.getTelephone());
		op.setNote(orderPoint.getNote());
		op.setAddress(convertAddress(orderPoint));

		return op;
	}

	private Address convertAddress(OrderPointDTO orderPoint) throws ParseException{

		Address addr = new Address();

		addr.setStreet(orderPoint.getStreet());
		addr.setHouseNumber(orderPoint.getHouseNumber());
		addr.setPostalCode(orderPoint.getPostalcode());
		addr.setCity(orderPoint.getCity());

		return addr;
	}

	private List<cz.cvut.a4m36jee.messe.model.order.Package> convertPackages(List<PackageDTO> packages) throws ParseException{
		List<cz.cvut.a4m36jee.messe.model.order.Package> pac = new ArrayList<>();

		for(PackageDTO dto : packages){
			cz.cvut.a4m36jee.messe.model.order.Package p = new cz.cvut.a4m36jee.messe.model.order.Package();
			p.setWeight(Integer.parseInt(dto.getWeight()));
			p.setHeight(Integer.parseInt(dto.getHeight()));
			p.setWidth(Integer.parseInt(dto.getWidth()));
			p.setDepth(Integer.parseInt(dto.getDepth()));
			pac.add(p);
		}
		return pac;
	}
}