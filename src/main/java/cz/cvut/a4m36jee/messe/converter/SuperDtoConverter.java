package cz.cvut.a4m36jee.messe.converter;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public abstract class SuperDtoConverter<Clazz, Dto> {
    
	public abstract Dto convert(Clazz input);

	public Clazz convertDto(Dto input) throws ParseException {
		throw new NotImplementedException();
	}
	
	public List<Dto> convertToList(final List<Clazz> input) {
		List<Dto> converted = new ArrayList<>();
		
		if(input == null || input.size() == 0){
			return converted;
		}
		
		for(Clazz clazz : input){
			converted.add(convert(clazz));
		}
		
        return converted;
    }
}