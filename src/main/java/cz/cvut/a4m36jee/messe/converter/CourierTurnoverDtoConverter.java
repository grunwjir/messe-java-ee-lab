package cz.cvut.a4m36jee.messe.converter;

import cz.cvut.a4m36jee.messe.dto.turnover.CourierTurnoverDTO;
import cz.cvut.a4m36jee.messe.model.Turnover;
import cz.cvut.a4m36jee.messe.util.Util;

import javax.inject.Inject;

public class CourierTurnoverDtoConverter extends SuperDtoConverter<Turnover, CourierTurnoverDTO> {

	@Inject
	Util util;

	@Override
	public CourierTurnoverDTO convert(Turnover s) {
		if(s == null){
			return null;
		}
		return new CourierTurnoverDTO(
				s.getId().toString(),
				util.dateToDateTimeString(s.getCreated()),
				s.getCourier().getName(),
				s.getProfit().toPlainString(),
				s.getMessage(),
				s.getConsignmentSize().toString());
	}
}
