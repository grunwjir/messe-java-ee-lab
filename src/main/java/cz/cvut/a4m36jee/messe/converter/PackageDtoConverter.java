package cz.cvut.a4m36jee.messe.converter;

import cz.cvut.a4m36jee.messe.dto.order.PackageDTO;
import cz.cvut.a4m36jee.messe.model.order.*;

public class PackageDtoConverter extends SuperDtoConverter<cz.cvut.a4m36jee.messe.model.order.Package, PackageDTO> {

	@Override
	public PackageDTO convert(cz.cvut.a4m36jee.messe.model.order.Package p) {	
		if(p == null){
			return null;
		}
		return new PackageDTO(
				p.getId().toString(), 
				p.getWeight().toString(), 
				p.getHeight().toString(), 
				p.getWidth().toString(), 
				p.getDepth().toString());
	}

	@Override
	public cz.cvut.a4m36jee.messe.model.order.Package convertDto(PackageDTO input) {
		return null;
	}

}
