package cz.cvut.a4m36jee.messe.batch_jobs.turnover_generator;

import cz.cvut.a4m36jee.messe.data.MemberRepository;
import cz.cvut.a4m36jee.messe.model.Member;

import javax.batch.api.chunk.AbstractItemReader;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;


@Named
public class TurnoverReader extends AbstractItemReader {

    @Inject
    private MemberRepository repository;

    private Iterator<Member> courierListIterator;

    @Override
    public void open(Serializable checkpoint) throws Exception {
        List<Member> courierList = repository.findAllOrderedByName();//TODO find couriers
        courierListIterator = courierList.iterator();
    }

    @Override
    public Member readItem() {
        if (courierListIterator.hasNext()) {
            return courierListIterator.next();
        } else {
            return null;
        }
    }
}
