package cz.cvut.a4m36jee.messe.batch_jobs.turnover_generator;

import cz.cvut.a4m36jee.messe.service.TurnoverService;

import javax.batch.api.chunk.AbstractItemWriter;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;


@Named
public class TurnoverWriter extends AbstractItemWriter {
    
    @Inject
    TurnoverService turnoverService;

    @Override
    public void writeItems(List list) {
        for (TurnoverJobResult turnoverJobResult : (List<TurnoverJobResult>) list) {
            turnoverService.saveTurnoverJobResult(turnoverJobResult);
        }
    }
}
