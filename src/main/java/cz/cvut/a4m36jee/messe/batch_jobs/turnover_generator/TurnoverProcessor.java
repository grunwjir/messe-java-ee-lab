package cz.cvut.a4m36jee.messe.batch_jobs.turnover_generator;

import cz.cvut.a4m36jee.messe.data.product.OrderRepository;
import cz.cvut.a4m36jee.messe.model.Address;
import cz.cvut.a4m36jee.messe.model.Member;
import cz.cvut.a4m36jee.messe.model.Turnover;
import cz.cvut.a4m36jee.messe.model.order.Delivery;
import cz.cvut.a4m36jee.messe.util.Util;

import javax.batch.api.chunk.ItemProcessor;
import javax.batch.runtime.BatchRuntime;
import javax.batch.runtime.context.JobContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Properties;


@Named
public class TurnoverProcessor implements ItemProcessor {
    public static final String FROM_DATE_KEY = "FROM_DATE_KEY";
    public static final String TO_DATE_KEY = "TO_DATE_KEY";

    @Inject
    private OrderRepository orderRepository;

    @Inject
    private JobContext jobCtx;
    private Date fromDate;
    private Date toDate;

    @Inject
    Util util;

    @Override
    public TurnoverJobResult processItem(Object t) {
        Member messenger = (Member) t;
        if (fromDate == null || toDate == null) {
            Properties runtimeParams = BatchRuntime.getJobOperator().getParameters(jobCtx.getExecutionId());
            fromDate = util.stringToDate(runtimeParams.getProperty(FROM_DATE_KEY));
            toDate = util.stringToDate(runtimeParams.getProperty(TO_DATE_KEY));
        }
        List<Delivery> completedDeliveries = orderRepository.findDeliveredNotAccountedOrdersOfMessengerInPeriod(messenger, fromDate, toDate);
        if (completedDeliveries.isEmpty()) {//do not create turnover summary if there are no deliveries
            return null;
        }
        BigDecimal deliveriesPriceSum = BigDecimal.ZERO;
        StringBuilder sb = new StringBuilder();
        sb.append("Summary of deliveries from date " + fromDate + " to date " + toDate + " \n\n");
        sb.append("Name: " + messenger.getName() + "\n");
        sb.append("Email: " + messenger.getEmail() + "\n");
        sb.append("\n\n");
        sb.append("Delivered packets:\n");
        for (Delivery delivery : completedDeliveries) {
            deliveriesPriceSum = deliveriesPriceSum.add(delivery.getPrice());
            Address pickupAddress = delivery.getPickupPoint().getAddress();
            Address deliveryAddress = delivery.getDeliveryPoint().getAddress();
            sb.append(delivery.getId() + "\t" + delivery.getProduct().getName() + "\t" + pickupAddress.getAddressLine1() + "," + pickupAddress.getCity() + "\t" +
                    deliveryAddress.getAddressLine1() + "," + deliveryAddress.getCity() + "\t" + delivery.getPrice() + "\n");
        }
        sb.append("\n");
        sb.append("Total price: " + deliveriesPriceSum + "\n");
        BigDecimal profit = deliveriesPriceSum.multiply(messenger.getFee());
        sb.append("Earning: " + profit);
        Turnover turnover = new Turnover();
        turnover.setProfit(profit);
        turnover.setConsignmentSize(completedDeliveries.size());
        turnover.setCreated(new Date());
        turnover.setCourier(messenger);
        turnover.setMessage(sb.toString());
        return new TurnoverJobResult(turnover,completedDeliveries);
    }
}
