package cz.cvut.a4m36jee.messe.batch_jobs.turnover_generator;

import cz.cvut.a4m36jee.messe.model.Turnover;
import cz.cvut.a4m36jee.messe.model.order.Delivery;

import java.util.List;

/**
 * Created by frox on 17.6.16.
 */
public class TurnoverJobResult {
    private Turnover turnover;
    private  List<Delivery> deliveryList;

    public TurnoverJobResult(Turnover turnover, List<Delivery> deliveryList) {
        this.turnover = turnover;
        this.deliveryList = deliveryList;
    }

    public Turnover getTurnover() {
        return turnover;
    }

    public List<Delivery> getDeliveryList() {
        return deliveryList;
    }
}
