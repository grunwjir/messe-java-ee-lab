package cz.cvut.a4m36jee.messe.jms.appmanaged;

import cz.cvut.a4m36jee.messe.jms.JMSResources;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.DependsOn;
import javax.ejb.Singleton;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.*;

@Stateless
public class MessageReceiverAppManaged {

    @Inject
    JMSContext jmsContext;

    @Resource(mappedName = JMSResources.SYNC_APP_MANAGED_QUEUE)
    Queue myQueue;

    public String receiveMessage() {
        return jmsContext.createConsumer(myQueue).receiveBody(String.class);
    }
}