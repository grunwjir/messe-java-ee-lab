package cz.cvut.a4m36jee.messe.jms.mdb;


import cz.cvut.a4m36jee.messe.jms.JMSResources;

import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.Queue;

@Stateless
public class MessageSenderAsyncQueue {

    @Inject
    JMSContext jmsContext;

    @Resource(mappedName = JMSResources.ASYNC_QUEUE)
    Queue myQueue;

    public void sendMessage(String message) {
        jmsContext.createProducer().send(myQueue, message);
    }
}