package cz.cvut.a4m36jee.messe.jms;

import javax.jms.JMSDestinationDefinition;
import javax.jms.JMSDestinationDefinitions;

@JMSDestinationDefinitions({
    @JMSDestinationDefinition(name = JMSResources.ASYNC_QUEUE,
        resourceAdapter = "jmsra",
        interfaceName = "javax.jms.Queue",
        destinationName = "asyncQueue",
        description = "My Async Queue"),
    @JMSDestinationDefinition(name = JMSResources.SYNC_APP_MANAGED_QUEUE,
        resourceAdapter = "jmsra",
        interfaceName = "javax.jms.Queue",
        destinationName = "syncAppQueue",
        description = "My Sync Queue for App-managed JMSContext"),
})
public class JMSResources {
    public static final String SYNC_APP_MANAGED_QUEUE = "java:global/jms/messeSyncAppQueue";
    public static final String ASYNC_QUEUE = "java:global/jms/messeAsyncQueue";
}
