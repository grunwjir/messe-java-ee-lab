package cz.cvut.a4m36jee.messe.jms.appmanaged;


import cz.cvut.a4m36jee.messe.jms.JMSResources;

import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.*;

@Stateless
public class MessageSenderAppManaged {

    @Inject
    JMSContext jmsContext;

    @Resource(mappedName = JMSResources.SYNC_APP_MANAGED_QUEUE)
    Queue myQueue;

    public void sendMessage(String message) {
        jmsContext.createProducer().send(myQueue, message);
    }
}
