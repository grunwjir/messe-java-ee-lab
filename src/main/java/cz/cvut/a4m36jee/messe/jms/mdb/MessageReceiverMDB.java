package cz.cvut.a4m36jee.messe.jms.mdb;

import cz.cvut.a4m36jee.messe.jms.JMSResources;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.util.logging.Logger;

@MessageDriven(activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationLookup",
                propertyValue = JMSResources.ASYNC_QUEUE),
        @ActivationConfigProperty(propertyName = "destinationType",
                propertyValue = "javax.jms.Queue"),
})
public class MessageReceiverMDB implements MessageListener {

    @Inject
    private Logger logger;

    @Override
    public void onMessage(Message message) {
        try {
            TextMessage tm = (TextMessage) message;
            logger.info("Received and processed a message: " + tm.getText());
        } catch (JMSException ex) {
            logger.severe("Can't process an incoming message!" + ex);
        }
    }
}