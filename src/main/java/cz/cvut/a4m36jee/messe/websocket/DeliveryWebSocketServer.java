package cz.cvut.a4m36jee.messe.websocket;

import cz.cvut.a4m36jee.messe.model.order.Delivery;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;

@ServerEndpoint("/deliveries")
@ApplicationScoped
public class DeliveryWebSocketServer {

    @Inject
    DeliverySessionHandler sessionHandler;

    @OnOpen
    public void open(Session session) {
        sessionHandler.addSession(session);
    }

    @OnClose
    public void close(Session session) {
        sessionHandler.removeSession(session);
    }

    @OnError
    public void onError(Throwable error) {
        Logger.getLogger(DeliveryWebSocketServer.class.getName()).log(Level.SEVERE, null, error);
    }

    @OnMessage
    public void handleMessage(String message, Session session) {

        try (JsonReader reader = Json.createReader(new StringReader(message))) {
            JsonObject jsonMessage = reader.readObject();

            if ("update".equals(jsonMessage.getString("action"))) {
                sessionHandler.sendUpdate();
            }
        }
    }

    /**
     * Prijima eventy vyvolane pri vytvoreni/zmene objednavky
     *
     * @param delivery
     */
    public void onChange(@Observes Delivery delivery) {
        sessionHandler.sendUpdate();
    }

}