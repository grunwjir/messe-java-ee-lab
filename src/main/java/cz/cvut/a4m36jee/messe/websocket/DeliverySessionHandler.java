package cz.cvut.a4m36jee.messe.websocket;

import cz.cvut.a4m36jee.messe.converter.OrderDtoConverter;
import cz.cvut.a4m36jee.messe.data.product.OrderRepository;
import javax.enterprise.context.ApplicationScoped;
import java.io.IOException;
import java.util.HashSet;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.json.spi.JsonProvider;
import javax.websocket.Session;

@ApplicationScoped
public class DeliverySessionHandler {

    @Inject
    OrderDtoConverter orderDtoConverter;

    @Inject
    OrderRepository orderRepository;

    private final HashSet<Session> sessions = new HashSet<>();

    public void addSession(Session session) {
        sessions.add(session);
    }

    public void removeSession(Session session) {
        sessions.remove(session);
    }

    public void sendUpdate() {
        JsonObject addMessage = createAddMessage();
        sendToAllConnectedSessions(addMessage);
    }

    private JsonObject createAddMessage() {
        JsonProvider provider = JsonProvider.provider();
        JsonObject addMessage = provider.createObjectBuilder()
                .add("action", "update")
                .build();
        return addMessage;
    }

    private void sendToAllConnectedSessions(JsonObject message) {
        for (Session session : sessions) {
            sendToSession(session, message);
        }
    }

    private void sendToSession(Session session, JsonObject message) {
        try {
            session.getBasicRemote().sendText(message.toString());
        } catch (IOException ex) {
            sessions.remove(session);
            Logger.getLogger(DeliverySessionHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}