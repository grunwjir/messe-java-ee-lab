package cz.cvut.a4m36jee.messe.util;

import javax.inject.Inject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Util {

    @Inject
    @DateFormatter
    SimpleDateFormat dateFormatter;

    @Inject
    @DateTimeFormatter
    SimpleDateFormat dateTimeFormatter;

    public Date stringToDate(String dateStr) {
        try {
            return dateFormatter.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String dateToDateTimeString(Date created) {
        if (created == null) {
            return null;
        } else {
            return dateTimeFormatter.format(created);
        }
    }
}
