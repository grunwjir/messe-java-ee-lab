/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cvut.a4m36jee.messe.util;

import cz.cvut.a4m36jee.messe.data.MemberRepository;
import cz.cvut.a4m36jee.messe.model.Member;
import cz.cvut.a4m36jee.messe.service.MemberService;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.logging.Logger;

/**
 * This class uses CDI to alias Java EE resources, such as the persistence context, to CDI beans
 * 
 * <p>
 * Example injection on a managed bean field:
 * </p>
 * 
 * <pre>
 * &#064;Inject
 * private EntityManager em;
 * </pre>
 */
public class Resources {

    @PersistenceContext
    private EntityManager em;

    @Inject
    private MemberRepository memberRepository;

    @Produces
    @MesseDatabase
    public EntityManager getEm() {
        return em;
    }

    @Inject
    Principal principal;

    @Produces
    @LoggedIn
    public Member getLoggedInMember() {
        if (principal.getName() == null) {
            return null;
        }
        Member member = memberRepository.findByEmail(principal.getName());
        return member;
    }

    @Produces
    @DateFormatter
    @Singleton
    public SimpleDateFormat getDateFormatter() {
        return new SimpleDateFormat("dd.MM.yyyy");
    }

    @Produces
    @DateTimeFormatter
    @Singleton
    public SimpleDateFormat getDateTimeFormatter() {
        return new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
    }

    @Produces
    public Logger produceLog(InjectionPoint injectionPoint) {
        return Logger.getLogger(injectionPoint.getMember().getDeclaringClass().getName());
    }

    @Produces
    @RequestScoped
    public FacesContext produceFacesContext() {
        return FacesContext.getCurrentInstance();
    }

}
