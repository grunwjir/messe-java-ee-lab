/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cvut.a4m36jee.messe.data;

import cz.cvut.a4m36jee.messe.model.Member;
import cz.cvut.a4m36jee.messe.model.Turnover;
import cz.cvut.a4m36jee.messe.util.MesseDatabase;

import javax.ejb.Stateless;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Stateless
public class TurnoverRepository {

    @Inject
    @MesseDatabase
    private EntityManager em;

    public List<Turnover> findAllOrderedById() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Turnover> criteria = cb.createQuery(Turnover.class);
        Root<Turnover> summary = criteria.from(Turnover.class);
        criteria.select(summary).orderBy(cb.asc(summary.get("id")));
        return em.createQuery(criteria).getResultList();
    }

    public List<Turnover> findAllOrderedById(Member courier) {
        return em.createNamedQuery("Turnover.findAllForCourier", Turnover.class).setParameter("courier", courier).getResultList();
    }

    public void save(Turnover turnover) {
        em.merge(turnover);
    }
}
