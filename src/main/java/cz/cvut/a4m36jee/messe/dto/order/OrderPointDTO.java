package cz.cvut.a4m36jee.messe.dto.order;

public class OrderPointDTO {
	
	private String name;
	
	private String telephone;
	
	private String note;
	
	private String street;
	
	private String postalcode;
	
	private String houseNumber;
	
	private String city;
	
	private String dateFrom;
	
	private String dateTo;
	
	public OrderPointDTO() {
	}
	
	public OrderPointDTO(String name, String telephone, String note, String street, String postalcode,
			String houseNumber, String city, String dateFrom, String dateTo) {
		super();
		this.name = name;
		this.telephone = telephone;
		this.note = note;
		this.street = street;
		this.postalcode = postalcode;
		this.houseNumber = houseNumber;
		this.city = city;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getPostalcode() {
		return postalcode;
	}

	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
}
