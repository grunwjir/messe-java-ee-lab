package cz.cvut.a4m36jee.messe.dto.order;

import java.util.List;

public class OrderDTO {

	private String id;

	private String state;

	private String code;

	private String price;
	
	private String product;
	
	private String customer;
	
	private String courier;
	
	private String productName;

	private String stateName;

	private String customerName;
	
	private String courierName;
	
	private List<PackageDTO> packages;
	
	private OrderPointDTO pickup;
	
	private OrderPointDTO delivery;
	
	public OrderDTO() {
	}
	
	public OrderDTO(String id, String state, String code, String price, String product, String customer, String courier,
			List<PackageDTO> packages, OrderPointDTO pickup, OrderPointDTO delivery, String productName, String customerName, String courierName) {
		super();
		this.id = id;
		this.state = state;
		this.code = code;
		this.price = price;
		this.product = product;
		this.customer = customer;
		this.courier = courier;
		this.packages = packages;
		this.pickup = pickup;
		this.delivery = delivery;
		this.customerName = customerName;
		this.courierName = courierName;
		this.productName = productName;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public List<PackageDTO> getPackages() {
		return packages;
	}
	public void setPackages(List<PackageDTO> packages) {
		this.packages = packages;
	}

	public OrderPointDTO getPickup() {
		return pickup;
	}

	public void setPickup(OrderPointDTO pickup) {
		this.pickup = pickup;
	}

	public OrderPointDTO getDelivery() {
		return delivery;
	}

	public void setDelivery(OrderPointDTO delivery) {
		this.delivery = delivery;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getCourier() {
		return courier;
	}

	public void setCourier(String courier) {
		this.courier = courier;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCourierName() {
		return courierName;
	}

	public void setCourierName(String courierName) {
		this.courierName = courierName;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
}
