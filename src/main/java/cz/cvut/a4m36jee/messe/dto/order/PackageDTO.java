package cz.cvut.a4m36jee.messe.dto.order;

public class PackageDTO {
	
	private String id;
	
	private String weight;
	
	private String height;
	
	private String width;
	
	private String depth;
	
	public PackageDTO() {
	} 
	
	public PackageDTO(String id, String weight, String height, String width, String depth) {
		super();
		this.id = id;
		this.weight = weight;
		this.height = height;
		this.width = width;
		this.depth = depth;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getWidth() {
		return width;
	}
	public void setWidth(String width) {
		this.width = width;
	}
	public String getDepth() {
		return depth;
	}
	public void setDepth(String depth) {
		this.depth = depth;
	}
	
	
	
}
