package cz.cvut.a4m36jee.messe.dto.order;

public class ProductDTO {
	private String id;
	private String name;
	private String pricePerKm;
	private String basePrice;
	
	public ProductDTO(String id, String name, String pricePerKm, String basePrice) {
		super();
		this.id = id;
		this.name = name;
		this.pricePerKm = pricePerKm;
		this.basePrice = basePrice;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPricePerKm() {
		return pricePerKm;
	}
	public void setPricePerKm(String pricePerKm) {
		this.pricePerKm = pricePerKm;
	}
	public String getBasePrice() {
		return basePrice;
	}
	public void setBasePrice(String basePrice) {
		this.basePrice = basePrice;
	}
	
	
	
}
