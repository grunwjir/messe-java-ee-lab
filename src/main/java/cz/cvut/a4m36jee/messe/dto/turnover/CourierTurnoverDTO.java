package cz.cvut.a4m36jee.messe.dto.turnover;

public class CourierTurnoverDTO {
	
	private String id;
	private String created;
	private String courierName;
	private String profit;
	private String message;
	private String consignmentCount;
	
	public CourierTurnoverDTO(String id, String created, String courierName, String profit, String message, String consignmentCount) {
		this.id = id;
		this.created = created;
		this.courierName = courierName;
		this.profit = profit;
		this.message = message;
		this.consignmentCount = consignmentCount;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getCourierName() {
		return courierName;
	}
	public void setCourierName(String courierName) {
		this.courierName = courierName;
	}
	public String getProfit() {
		return profit;
	}
	public void setProfit(String profit) {
		this.profit = profit;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getConsignmentCount() {
		return consignmentCount;
	}
	public void setConsignmentCount(String consignmentCount) {
		this.consignmentCount = consignmentCount;
	}
	
}
