package cz.cvut.a4m36jee.messe.service;


import cz.cvut.a4m36jee.messe.dto.order.OrderDTO;
import cz.cvut.a4m36jee.messe.jms.appmanaged.MessageSenderAppManaged;
import cz.cvut.a4m36jee.messe.jms.mdb.MessageSenderAsyncQueue;

import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class JMSLogOrderService {

    @Inject
    private MessageSenderAsyncQueue messageSenderAsyncQueue;

    public void logOrder(OrderDTO orderDTO) {
        messageSenderAsyncQueue.sendMessage("OrderID: " + orderDTO.getCode());
    }

}