package cz.cvut.a4m36jee.messe.service;

import cz.cvut.a4m36jee.messe.data.MemberRepository;
import cz.cvut.a4m36jee.messe.model.Member;
import cz.cvut.a4m36jee.messe.model.Role;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.security.Principal;
import java.util.List;

@ApplicationScoped
public class MemberService {
    @Inject
    MemberRepository memberRepository;

    public void createMember(Member member) {
        updateMember(member);
    }

    public void updateMember(Member member) {
        if (hasValidRoles(member)) {
            memberRepository.save(member);
        } else {
            throw new IllegalArgumentException("Member has invalid role.");
        }
    }

    private boolean hasValidRoles(Member member) {
        for (Role r : member.getRoles()) {
            if (!Role.isValidRole(r)) {
                return false;
            }
        }
        return true;
    }

    public List<Member> findAllByRole(Role role) {
        return memberRepository.findAllByRole(role);
    }

    public List<Member> findAllOrderedByName() {
        return memberRepository.findAllOrderedByName();
    }

    public Member findById(Long id) {
        return memberRepository.findById(id);
    }

    public List<String> getAllRoles() {
        return memberRepository.getAllRoles();
    }

    public Member findByEmail(String email) {
        return memberRepository.findByEmail(email);
    }
}

