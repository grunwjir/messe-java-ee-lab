package cz.cvut.a4m36jee.messe.service;

import cz.cvut.a4m36jee.messe.model.product.Product;
import org.json.JSONException;
import org.json.JSONObject;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import java.math.BigDecimal;

@ApplicationScoped
public class PriceService {

	private static final String apikey = "AIzaSyBGbCu8nYsn1kKvv3HF9JTVDp112Tuz-tU";
	public static final String URL = "https://maps.googleapis.com/maps/api/distancematrix/json";

	public BigDecimal compute(String origins, String destinations, Product product){

		if(product == null){
			return null;
		}
		
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(URL).queryParam("key", apikey);
		
		String response = target.queryParam("origins", origins)
				.queryParam("destinations", destinations).request().get().readEntity(String.class);

		try {
			JSONObject json = new JSONObject(response);
			String distance = json.getJSONArray("rows").getJSONObject(0).getJSONArray("elements").getJSONObject(0).getJSONObject("distance").get("value").toString();
			BigDecimal b = new BigDecimal(distance);
			return b.divide(new BigDecimal(1000)).multiply(product.getPricePerKm()).add(product.getBasePrice());
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}
}
