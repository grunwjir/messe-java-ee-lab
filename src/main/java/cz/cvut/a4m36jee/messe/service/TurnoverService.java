package cz.cvut.a4m36jee.messe.service;

import cz.cvut.a4m36jee.messe.batch_jobs.turnover_generator.TurnoverJobResult;
import cz.cvut.a4m36jee.messe.batch_jobs.turnover_generator.TurnoverProcessor;
import cz.cvut.a4m36jee.messe.data.TurnoverRepository;
import cz.cvut.a4m36jee.messe.model.Member;
import cz.cvut.a4m36jee.messe.model.Turnover;
import cz.cvut.a4m36jee.messe.model.order.Delivery;

import javax.batch.operations.JobOperator;
import javax.batch.runtime.BatchRuntime;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Properties;

@ApplicationScoped
public class TurnoverService {
    @Inject
    private TurnoverRepository turnoverRepository;

    @Inject
    private OrderService orderService;

    public void generateTurnovers(String fromDate, String toDate) {
        JobOperator jo = BatchRuntime.getJobOperator();
        Properties runtimeParameters = new Properties();
        runtimeParameters.setProperty(TurnoverProcessor.FROM_DATE_KEY, fromDate);
        runtimeParameters.setProperty(TurnoverProcessor.TO_DATE_KEY, toDate);
        long jid = jo.start("createTurnovers", runtimeParameters);
        System.out.println("Job submitted: " + jid + "<br>");
    }

    public List<Turnover> findAllForUser(Member member) {
        return turnoverRepository.findAllOrderedById(member);
    }

    public List<Turnover> findAllOrderedById() {
        return turnoverRepository.findAllOrderedById();
    }

    public void saveTurnoverJobResult(TurnoverJobResult turnoverJobResult) {
        for (Delivery delivery : turnoverJobResult.getDeliveryList()) {
            delivery.setIsAccounted(Boolean.TRUE);
        }
        turnoverRepository.save(turnoverJobResult.getTurnover());
    }
}
