package cz.cvut.a4m36jee.messe.service;

import cz.cvut.a4m36jee.messe.data.product.OrderRepository;
import cz.cvut.a4m36jee.messe.model.Member;
import cz.cvut.a4m36jee.messe.model.order.Delivery;
import cz.cvut.a4m36jee.messe.model.order.OrderState;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import java.util.List;

@ApplicationScoped
public class OrderService {
    @Inject
    OrderService orderService;

    @Inject
    OrderRepository orderRepository;

    @Inject
    private Event<Delivery> orderEvent;

    @Inject
    PriceService priceService;

    public void createOrder(Delivery delivery) {
        if (delivery.getCourier() == null) {
            delivery.setState(OrderState.CREATED);
        } else {
            delivery.setState(OrderState.ASSIGNED);
        }
        delivery.setPrice(priceService.compute(delivery.getPickupPoint().getAddress().getAddressLine1(),
                delivery.getDeliveryPoint().getAddress().getAddressLine1(), delivery.getProduct()));
        orderRepository.save(delivery);
        orderEvent.fire(delivery);
    }

    public Delivery findOrder(Long id) {
        return orderRepository.findById(id);
    }

    public void updateOrder(Delivery delivery) {
        orderRepository.save(delivery);
        orderEvent.fire(delivery);
    }

    public List<Delivery> findAllOrderedById() {
        return orderRepository.findAllOrderedById();
    }

    public List<Delivery> findUndeliveredOrders(Member loggedInMember) {
        return orderRepository.findUndeliveredOrders(loggedInMember);
    }
}

