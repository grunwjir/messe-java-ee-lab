/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cvut.a4m36jee.messe.rest;

import cz.cvut.a4m36jee.messe.converter.OrderDtoConverter;
import cz.cvut.a4m36jee.messe.data.product.OrderRepository;
import cz.cvut.a4m36jee.messe.data.product.ProductRepository;
import cz.cvut.a4m36jee.messe.dto.order.OrderDTO;
import cz.cvut.a4m36jee.messe.model.Member;
import cz.cvut.a4m36jee.messe.model.Role;
import cz.cvut.a4m36jee.messe.model.order.Delivery;
import cz.cvut.a4m36jee.messe.model.order.OrderState;
import cz.cvut.a4m36jee.messe.service.JMSLogOrderService;
import cz.cvut.a4m36jee.messe.service.OrderService;
import cz.cvut.a4m36jee.messe.service.PriceService;
import cz.cvut.a4m36jee.messe.util.LoggedIn;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.criteria.Order;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.List;

@Path("/orders")
@RequestScoped
public class OrderResourceRESTService {

	@Inject 
	OrderDtoConverter orderDtoConverter;

    @Inject
    OrderService orderService;

    @Inject
    PriceService priceService;

    @Inject
    JMSLogOrderService jmsLogOrderService;

    @Inject
    @LoggedIn
    Member loggedInMember;

    @Inject
    private ProductRepository productRepository;

    @GET
    @RolesAllowed({Role.ADMIN_ROLE, Role.OPERATOR_ROLE})
    @Produces(MediaType.APPLICATION_JSON)
    public List<OrderDTO> listAllOrders() {
        return orderDtoConverter.convertToList(orderService.findAllOrderedById());
    }

    @POST
    @RolesAllowed({Role.ADMIN_ROLE, Role.OPERATOR_ROLE})
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createOrder(OrderDTO orderDto) {
        jmsLogOrderService.logOrder(orderDto);
        Response.ResponseBuilder builder;
        try {
            orderService.createOrder(orderDtoConverter.convertDto(orderDto));
            builder = Response.ok();
        } catch (Exception e) {
        	builder = Response.status(Response.Status.BAD_REQUEST);
			e.printStackTrace();
		}
        return builder.build();
    }

    @POST
    @RolesAllowed({Role.ADMIN_ROLE, Role.COURIER_ROLE})
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id:[0-9][0-9]*}/startDelivering")
    public Response startDelivering(@PathParam("id") Long id) {
        Delivery delivery = orderService.findOrder(id);
        if (delivery == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else if (delivery.getState() != OrderState.ASSIGNED) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        } else {
            delivery.setState(OrderState.DELIVERING);
            orderService.updateOrder(delivery);
            return Response.ok().build();
        }
    }

    @POST
    @RolesAllowed({Role.ADMIN_ROLE, Role.COURIER_ROLE})
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id:[0-9][0-9]*}/wasDelivered")
    public Response delivered(@PathParam("id") Long id) {
        Delivery delivery = orderService.findOrder(id);
        if (delivery == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else if (delivery.getState() != OrderState.DELIVERING) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        } else {
            delivery.setState(OrderState.DELIVERED);
            orderService.updateOrder(delivery);
            return Response.ok().build();
        }
    }

    @GET
    @RolesAllowed({Role.ADMIN_ROLE, Role.COURIER_ROLE})
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/mine/undelivered")
    public List<OrderDTO> mineUndeliveredOrders() {
        return orderDtoConverter.convertToList(orderService.findUndeliveredOrders(loggedInMember));
    }

    @GET
    @RolesAllowed({Role.ADMIN_ROLE, Role.OPERATOR_ROLE})
    @Path("/computePrice")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public BigDecimal computePrice(@QueryParam(value = "origins") String origins, @QueryParam(value = "destinations") String destinations, @QueryParam(value = "productId") String productId){
        return priceService.compute(origins, destinations, productRepository.findById(Long.parseLong(productId)));
    }
    

}
