/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cvut.a4m36jee.messe.rest;

import cz.cvut.a4m36jee.messe.converter.ProductDtoConverter;
import cz.cvut.a4m36jee.messe.data.product.ProductRepository;
import cz.cvut.a4m36jee.messe.dto.order.ProductDTO;
import cz.cvut.a4m36jee.messe.model.Role;
import cz.cvut.a4m36jee.messe.model.product.Product;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/products")
@RequestScoped
public class ProductResourceRESTService {

    @Inject
    private ProductRepository repository;

    @Inject
    private ProductDtoConverter productConverter;
    
    @GET
    @RolesAllowed({Role.ADMIN_ROLE, Role.OPERATOR_ROLE})
    @Produces(MediaType.APPLICATION_JSON)
    public List<ProductDTO> listAllProducts() {     
    	List<Product> products = repository.findAllOrderedById();
   
    	return productConverter.convertToList(products);
    }


}
