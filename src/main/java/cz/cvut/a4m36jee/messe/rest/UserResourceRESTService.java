/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cvut.a4m36jee.messe.rest;

import cz.cvut.a4m36jee.messe.converter.MemberDtoConverter;
import cz.cvut.a4m36jee.messe.dto.MemberDTO;
import cz.cvut.a4m36jee.messe.model.Member;
import cz.cvut.a4m36jee.messe.model.Role;
import cz.cvut.a4m36jee.messe.service.MemberService;
import cz.cvut.a4m36jee.messe.util.LoggedIn;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Path("/users")
@RequestScoped
public class UserResourceRESTService {

    @Inject
    private MemberService memberService;

    @Inject
    private MemberDtoConverter memberDtoConverter;

    @Inject
    @LoggedIn
    Member loggedInMember;
    
    @Inject
    private Validator validator;

    @GET
    @Path("/me")
    @PermitAll
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLoggedInUser() {
        if (loggedInMember == null) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        } else {
            return Response.ok(memberDtoConverter.convert(loggedInMember)).build();
        }

    }

    @GET
    @RolesAllowed({Role.ADMIN_ROLE})
    @Produces(MediaType.APPLICATION_JSON)
    public List<MemberDTO> listAllUsers() {
        List<Member> customers = memberService.findAllOrderedByName();
        return memberDtoConverter.convertToList(customers);
    }
    
    @GET
    @RolesAllowed({Role.ADMIN_ROLE})
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public MemberDTO getUserById(@PathParam("id") Long id) {
        return memberDtoConverter.convert(memberService.findById(id));
    }

    @POST
    @RolesAllowed({Role.ADMIN_ROLE})
    @Produces(MediaType.APPLICATION_JSON)
    public Response createMember(MemberDTO memberDTO) {
    	Response.ResponseBuilder builder = null;
    	
    	Member m = memberDtoConverter.convertDto(memberDTO, new Member());	
    	try{
    		validateMember(m);
    		memberService.createMember(m);
    		builder = Response.ok();
    	}catch(ConstraintViolationException ce){
    		builder = createViolationResponse(ce.getConstraintViolations());
    	} catch (Exception e) {
            // handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
    		builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
    	}
    	
        return builder.build();
    }

    @PUT
    @RolesAllowed({Role.ADMIN_ROLE})
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id:[0-9][0-9]*}")
    public Response updateMember(@PathParam("id") Long id, MemberDTO memberDTO) {
        if (!id.toString().equals(memberDTO.getId())) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        
        Response.ResponseBuilder builder = null;
        Member m = memberDtoConverter.convertDto(memberDTO, memberService.findById(id));
        
        try{
    		validateMember(m);
    		 memberService.updateMember(m);
    		builder = Response.ok();
    	}catch(ConstraintViolationException ce){
    		builder = createViolationResponse(ce.getConstraintViolations());
    	} catch (Exception e) {
            // handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
    		builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
    	}
          
        return builder.build();
    }

    @GET
    @RolesAllowed({Role.ADMIN_ROLE})
    @Path("/admin")
    @Produces(MediaType.APPLICATION_JSON)
    public List<MemberDTO> getAllAdmins() {
        List<Member> customers = memberService.findAllByRole(new Role(Role.ADMIN_ROLE));
        return memberDtoConverter.convertToList(customers);
    }

    @GET
    @RolesAllowed({Role.ADMIN_ROLE})
    @Path("/operators")
    @Produces(MediaType.APPLICATION_JSON)
    public List<MemberDTO> getAllOperators() {
        List<Member> customers = memberService.findAllByRole(new Role(Role.OPERATOR_ROLE));
        return memberDtoConverter.convertToList(customers);
    }

    @GET
    @RolesAllowed({Role.ADMIN_ROLE, Role.OPERATOR_ROLE})
    @Path("/couriers")
    @Produces(MediaType.APPLICATION_JSON)
    public List<MemberDTO> listAllCouriers() {
        List<Member> customers = memberService.findAllByRole(new Role(Role.COURIER_ROLE));
        return memberDtoConverter.convertToList(customers);
    }

    @GET
    @RolesAllowed({Role.ADMIN_ROLE, Role.OPERATOR_ROLE})
    @Path("/customers")
    @Produces(MediaType.APPLICATION_JSON)
    public List<MemberDTO> listAllCustomers() {
        List<Member> customers = memberService.findAllByRole(new Role(Role.CUSTOMER_ROLE));
        return memberDtoConverter.convertToList(customers);
    }

    @GET
    @RolesAllowed({Role.ADMIN_ROLE})
    @Path("/roles")
    @Produces(MediaType.APPLICATION_JSON)
    public List<String> listAllRoles() {
        return memberService.getAllRoles();
    }
    
    private void validateMember(Member member) throws ConstraintViolationException, ValidationException {
    	Set<ConstraintViolation<Member>> violations = validator.validate(member);
    	
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(new HashSet<ConstraintViolation<?>>(violations));
        }
    }
    
    private Response.ResponseBuilder createViolationResponse(Set<ConstraintViolation<?>> violations) {
    	Map<String, String> responseObj = new HashMap<>();
    	
        for (ConstraintViolation<?> violation : violations) {
            responseObj.put(violation.getPropertyPath().toString(), violation.getMessage());
        }
    	
    	return Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
    }
}
