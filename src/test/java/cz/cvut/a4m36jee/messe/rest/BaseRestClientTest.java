package cz.cvut.a4m36jee.messe.rest;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.resteasy.client.jaxrs.BasicAuthentication;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.runner.RunWith;

import java.io.File;

public class BaseRestClientTest {

    protected  BasicAuthentication [] basicAuthList = new BasicAuthentication[] {adminBasicAuth(), operatorBasicAuth(), courierBasicAuth(), customerBasicAuth()};
    protected static final int ADMIN_IDX = 0;
    protected static final int OPERATOR_IDX = 1;
    protected static final int COURIER_IDX = 2;
    protected static final int CUSTOMER_IDX = 3;

    @Deployment(testable = false)
    public static WebArchive create() {
        return ShrinkWrap.create(WebArchive.class)
                .addPackages(true, "cz.cvut.a4m36jee.messe", "org.json")
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsResource("META-INF/batch-jobs/createTurnovers.xml")
                .addAsResource("import.sql")
                .addAsWebInfResource(new File("src/test/resources/test-web.xml"), "web.xml")
                .addAsWebInfResource(new File("src/main/webapp/WEB-INF/jboss-web.xml"))
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                        // Deploy our test datasource
                .addAsWebInfResource("test-ds.xml");
    }

    protected BasicAuthentication adminBasicAuth() {
        return new BasicAuthentication("james@gmail.com", "pas");
    }

    protected BasicAuthentication operatorBasicAuth() {
        return new BasicAuthentication("jonas@gmail.com", "pas");
    }

    protected BasicAuthentication courierBasicAuth() {
        return new BasicAuthentication("lionel@gmail.com", "pas");
    }

    protected BasicAuthentication customerBasicAuth() {
        return new BasicAuthentication("albert@gmail.com", "pas");
    }

    protected BasicAuthentication unknownBasicAuth() {
        return new BasicAuthentication("unknosdd", "sdsdsd");
    }
}
