package cz.cvut.a4m36jee.messe.rest;

import cz.cvut.a4m36jee.messe.TestUtil;
import cz.cvut.a4m36jee.messe.dto.MemberDTO;
import cz.cvut.a4m36jee.messe.dto.order.OrderDTO;
import cz.cvut.a4m36jee.messe.dto.order.ProductDTO;
import cz.cvut.a4m36jee.messe.model.order.Delivery;
import org.jboss.arquillian.extension.rest.client.ArquillianResteasyResource;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.resteasy.client.jaxrs.BasicAuthentication;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(Arquillian.class)
public class OrderRestTest extends BaseRestClientTest {

    private static final double DEVIATION_PERCENTAGE = 0.05;

    @Test
    public void orderLifecycleTest(@ArquillianResteasyResource WebTarget webTarget) throws IOException {
        /*
        vytvorit objednavku s kuryrem
        kuryrem ji oznacit pro dorucovani
        kuryrem ji dorucit
         */

        OrderDTO orderDTO = TestUtil.createObjectFromJson("{\"id\":null,\"code\":1466378876025,\"product\":\"0\",\"customer\":\"1\",\"courier\":\"2\",\"price\":0,\"pickup\":{\"name\":\"Jan Novák\",\"telephone\":\"795 878 266\",\"street\":\"Na lepším\",\"houseNumber\":\"8\",\"postalcode\":\"140 00\",\"city\":\"Praha\",\"dateFrom\":\"2016-06-19T23:27:56.025Z\",\"dateTo\":\"2016-06-19T23:27:56.025Z\"},\"delivery\":{\"name\":\"Milan Zelený\",\"telephone\":\"420 779 897\",\"street\":\"Thákurova\",\"houseNumber\":\"9\",\"postalcode\":\"160 00\",\"city\":\"Praha\",\"dateFrom\":\"2016-06-19T23:27:56.025Z\",\"dateTo\":\"2016-06-19T23:27:56.025Z\"},\"packages\":[{\"weight\":1000,\"height\":50,\"width\":50,\"depth\":50}]}", OrderDTO.class);
        //courier with id 2
        Response response = webTarget.path("/orders").register(adminBasicAuth()).request().post(Entity.json(orderDTO));
        assertEquals(200, response.getStatus());
        response.close();
        MemberDTO courier = webTarget.path("/users/2").register(adminBasicAuth()).request().get().readEntity(MemberDTO.class);
        response = webTarget.path("/users/me").register(new BasicAuthentication(courier.getEmail(), "pas")).request().get();
        assertEquals(200, response.getStatus());
        response.close();
        response = webTarget.path("/orders/22/startDelivering").register(new BasicAuthentication(courier.getEmail(), "pas")).request().post(Entity.text(""));
        assertEquals(200, response.getStatus());
        response.close();
        response = webTarget.path("/orders/22/wasDelivered").register(new BasicAuthentication(courier.getEmail(), "pas")).request().post(Entity.text(""));
        assertEquals(200, response.getStatus());
        response.close();
    }

    @Test
    public void computePriceTest(@ArquillianResteasyResource WebTarget webTarget) throws IOException {

        OrderDTO orderDTO = TestUtil.createObjectFromJson("{\"id\":null,\"code\":3336378876025," +
                "\"product\":\"0\",\"customer\":\"1\",\"courier\":\"2\",\"price\":0," +
                "\"pickup\":{\"name\":\"Jan Novák\",\"telephone\":\"795 878 266\",\"street\":\"Pod Parukářkou\",\"houseNumber\":\"6\",\"postalcode\":\"130 00\",\"city\":\"Praha\",\"dateFrom\":\"2016-06-19T23:27:56.025Z\",\"dateTo\":\"2016-06-19T23:27:56.025Z\"}," +
                "\"delivery\":{\"name\":\"Milan Zelený\",\"telephone\":\"420 779 897\",\"street\":\"Thákurova\",\"houseNumber\":\"9\",\"postalcode\":\"160 00\",\"city\":\"Praha\",\"dateFrom\":\"2016-06-19T23:27:56.025Z\",\"dateTo\":\"2016-06-19T23:27:56.025Z\"},\"packages\":[{\"weight\":1000,\"height\":50,\"width\":50,\"depth\":50}]}", OrderDTO.class);
        //courier with id 2
        Response response = webTarget.path("/orders").register(adminBasicAuth()).request().post(Entity.json(orderDTO));
        assertEquals(200, response.getStatus());
        response.close();

        List<ProductDTO> productDTOList = (List<ProductDTO>) webTarget.path("/products").register(operatorBasicAuth()).request().get().readEntity(List.class);
        assertEquals(3, productDTOList.size());

        int  standardProductId = 0;
        int expressProductId = 1;
        int extremeProductId = 2;

        BigDecimal price = webTarget.path("/orders/computePrice")
                .queryParam("origins", orderDTO.getPickup().getStreet() + " " + orderDTO.getPickup().getHouseNumber() + " " + orderDTO.getPickup().getCity())
                .queryParam("destinations", orderDTO.getDelivery().getStreet() + " " + orderDTO.getDelivery().getHouseNumber() + " " + orderDTO.getDelivery().getCity())
                .queryParam("productId", standardProductId)
                .register(operatorBasicAuth())
                .request().get().readEntity(BigDecimal.class);
        assertEquals(112.23, price.doubleValue(), DEVIATION_PERCENTAGE);

        price = webTarget.path("/orders/computePrice")
                .queryParam("origins", orderDTO.getPickup().getStreet() + " " + orderDTO.getPickup().getHouseNumber() + " " + orderDTO.getPickup().getCity())
                .queryParam("destinations", orderDTO.getDelivery().getStreet() + " " + orderDTO.getDelivery().getHouseNumber() + " " + orderDTO.getDelivery().getCity())
                .queryParam("productId", expressProductId)
                .register(operatorBasicAuth())
                .request().get().readEntity(BigDecimal.class);
        assertEquals(158.676, price.doubleValue(), DEVIATION_PERCENTAGE);

        price = webTarget.path("/orders/computePrice")
                .queryParam("origins", orderDTO.getPickup().getStreet() + " " + orderDTO.getPickup().getHouseNumber() + " " + orderDTO.getPickup().getCity())
                .queryParam("destinations", orderDTO.getDelivery().getStreet() + " " + orderDTO.getDelivery().getHouseNumber() + " " + orderDTO.getDelivery().getCity())
                .queryParam("productId", extremeProductId)
                .register(operatorBasicAuth())
                .request().get().readEntity(BigDecimal.class);
        assertEquals(273.345, price.doubleValue(), DEVIATION_PERCENTAGE);
    }

    @Test
    public void authTest(@ArquillianResteasyResource WebTarget webTarget) throws IOException {

        // GET /orders {A, O}
        Response response = webTarget.path("/orders").register(adminBasicAuth()).request().get();
        assertEquals(200, response.getStatus());
        response.close();

        response = webTarget.path("/orders").register(operatorBasicAuth()).request().get();
        assertEquals(200, response.getStatus());
        response.close();

        response = webTarget.path("/orders").register(courierBasicAuth()).request().get();
        assertEquals(403, response.getStatus());
        response.close();

        response = webTarget.path("/orders").register(customerBasicAuth()).request().get();
        assertEquals(403, response.getStatus());
        response.close();

        response = webTarget.path("/orders").register(unknownBasicAuth()).request().get();
        assertEquals(401, response.getStatus());
        response.close();
    }



}
