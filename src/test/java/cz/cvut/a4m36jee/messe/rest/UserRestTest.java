package cz.cvut.a4m36jee.messe.rest;

import cz.cvut.a4m36jee.messe.dto.MemberDTO;
import cz.cvut.a4m36jee.messe.model.Role;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.extension.rest.client.ArquillianResteasyResource;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.resteasy.client.jaxrs.BasicAuthentication;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import java.io.File;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(Arquillian.class)
public class UserRestTest extends BaseRestClientTest {

    @Test
    public void tryMeEndpoint(@ArquillianResteasyResource WebTarget webTarget) {
        MemberDTO memberDTO = webTarget.path("/users/me").register(new BasicAuthentication("james@gmail.com", "pas")).request().get().readEntity(MemberDTO.class);
        assertEquals("james@gmail.com", memberDTO.getEmail());

        memberDTO = webTarget.path("/users/me").register(new BasicAuthentication("lionel@gmail.com", "pas")).request().get().readEntity(MemberDTO.class);
        assertEquals("lionel@gmail.com", memberDTO.getEmail());
    }

    @Test
    public void testListUsers(@ArquillianResteasyResource WebTarget webTarget) {
        List<MemberDTO> memberDTOList = (List<MemberDTO>) webTarget.path("/users").register(new BasicAuthentication("james@gmail.com", "pas")).request().get().readEntity(List.class);
        assertEquals(4, memberDTOList.size());
    }

//    @Test
//    public void testListRoles(@ArquillianResteasyResource WebTarget webTarget) {
//        List<MemberDTO> memberDTOList = (List<MemberDTO>) webTarget.path("/users/operators").register(new BasicAuthentication("james@gmail.com", "pas")).request().get().readEntity(List.class);
//        for (MemberDTO memberDTO : memberDTOList) { gives exception! TODO
//            assertTrue(memberDTO.getRoles().contains(new Role(Role.OPERATOR_ROLE)));
//        }
//
//        memberDTOList = (List<MemberDTO>) webTarget.path("/users/customers").register(new BasicAuthentication("james@gmail.com", "pas")).request().get().readEntity(List.class);
//        for (MemberDTO memberDTO : memberDTOList) {
//            assertTrue(memberDTO.getRoles().contains(new Role(Role.CUSTOMER_ROLE)));
//        }
//    }

    @Test
    public void testUpdateMember(@ArquillianResteasyResource WebTarget webTarget) {
        MemberDTO memberDTO = webTarget.path("/users/1").register(new BasicAuthentication("james@gmail.com", "pas")).request().get().readEntity(MemberDTO.class);
        memberDTO.setEmail("changed@gmail.com");
        webTarget.path("/users/1").register(new BasicAuthentication("james@gmail.com", "pas")).request().put(Entity.json(memberDTO)).close();
        memberDTO = webTarget.path("/users/1").register(adminBasicAuth()).request().get().readEntity(MemberDTO.class);
        assertEquals(memberDTO.getEmail(), "changed@gmail.com");
    }

}
