package cz.cvut.a4m36jee.messe.data;

import cz.cvut.a4m36jee.messe.BaseArquillianTest;
import cz.cvut.a4m36jee.messe.model.Member;
import cz.cvut.a4m36jee.messe.model.Role;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(Arquillian.class)
public class MemberRepositoryTest extends BaseArquillianTest{

    @Inject
    MemberRepository memberRepository;

    @Inject
    Logger log;

    private int memberCount;

    @Before
    public void getMemberCount() {
        memberCount = memberRepository.findAllOrderedByName().size();
    }

    @Test
    public void testFindById() throws Exception {
        Member memberFound = memberRepository.findById(1L);
        assertNotNull(memberFound);
    }

    @Test
    public void testFindByEmail() throws Exception {
        Member memberFound = memberRepository.findByEmail("jonas@gmail.com");
        assertNotNull(memberFound);
    }

    @Test
    public void testFindAllOrderedByName() throws Exception {
        int memberCount = memberRepository.findAllOrderedByName().size();
        assertEquals(memberCount, 4);
    }

    @Test
    public void testFindAllByRole() throws Exception {
        assertNotNull(memberRepository.findAllByRole(new Role(Role.ADMIN_ROLE)));
    }

    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testSave() throws Exception {
        log.info("testSave");
        Member newMember = new Member();
        newMember.setName("Jane Doe");
        newMember.setEmail("jane.doe1@mailinator.com");
        newMember.setPhoneNumber("2125551234");
        newMember.setPassword("BestPasswordEver1234");
        memberRepository.save(newMember);
        assertNotNull(memberRepository.findByEmail("jane.doe1@mailinator.com").getId());
        assertEquals(memberRepository.findAllOrderedByName().size(), memberCount + 1);
        log.info(newMember.getName() + " was persisted with id " + newMember.getId());
    }

    @After
    public void checkIfTransactionWasRolledBack() {
        assertEquals(memberCount, memberRepository.findAllOrderedByName().size());
    }
}
