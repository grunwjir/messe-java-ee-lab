package cz.cvut.a4m36jee.messe.jms;

import cz.cvut.a4m36jee.messe.BaseArquillianTest;
import cz.cvut.a4m36jee.messe.jms.appmanaged.MessageReceiverAppManaged;
import cz.cvut.a4m36jee.messe.jms.appmanaged.MessageSenderAppManaged;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueBrowser;

import static org.junit.Assert.assertEquals;

@RunWith(Arquillian.class)
public class AppManagedJMSQueueTest extends BaseArquillianTest {

    @EJB
    private MessageSenderAppManaged messageSenderAppManaged;

    @EJB
    private MessageReceiverAppManaged messageReceiverAppManaged;

    @Inject
    private JMSContext context;

    @Resource(mappedName = JMSResources.SYNC_APP_MANAGED_QUEUE)
    Queue myQueue;


    @Before
    public void clearQueue() throws JMSException {
        QueueBrowser browser = context.createBrowser(myQueue);
        while (browser.getEnumeration().hasMoreElements()) {
            messageReceiverAppManaged.receiveMessage();
        }
    }

    @Test
    public void testSendReceive() {
        String message = "A message";
        messageSenderAppManaged.sendMessage(message);

        assertEquals(message, messageReceiverAppManaged.receiveMessage());
    }

    @Test
    public void testMultipleSendAndReceive() {
        messageSenderAppManaged.sendMessage("1");
        messageSenderAppManaged.sendMessage("2");
        assertEquals("1", messageReceiverAppManaged.receiveMessage());
        assertEquals("2", messageReceiverAppManaged.receiveMessage());
        messageSenderAppManaged.sendMessage("3");
        messageSenderAppManaged.sendMessage("4");
        messageSenderAppManaged.sendMessage("5");
        assertEquals("3", messageReceiverAppManaged.receiveMessage());
        assertEquals("4", messageReceiverAppManaged.receiveMessage());
        assertEquals("5", messageReceiverAppManaged.receiveMessage());
    }

}