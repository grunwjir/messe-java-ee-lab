package cz.cvut.a4m36jee.messe;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

/**
 * Created by frox on 20.6.16.
 */
public class TestUtil {
    public static <T> T createObjectFromJson(String json, Class<T> desiredClass) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, desiredClass);
    }
}
