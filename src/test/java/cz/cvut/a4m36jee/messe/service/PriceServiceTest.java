package cz.cvut.a4m36jee.messe.service;

import cz.cvut.a4m36jee.messe.BaseArquillianTest;
import cz.cvut.a4m36jee.messe.model.product.Product;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.logging.Logger;

import static org.junit.Assert.assertTrue;

@RunWith(Arquillian.class)
public class PriceServiceTest extends BaseArquillianTest {
    @Inject
    PriceService priceService;

    @Inject
    Logger logger;

    private static final double DEVIATION_PERCENTAGE = 0.05;

    @Test
    public void testComputePrice() {
        Product product = new Product();
        product.setPricePerKm(BigDecimal.valueOf(12.5));
        product.setBasePrice(BigDecimal.valueOf(10.5));
        BigDecimal price = priceService.compute("Dvouramenná 8", "Karlovo nám. 13, Praha", product);
        assertRounded(79.2, price.doubleValue(), DEVIATION_PERCENTAGE);

        price = priceService.compute("Václavské nám. 59, Praha", "Hermelínská 1203/6, Praha", product);
        System.out.println("vaclavak price: " + price.doubleValue());
        assertRounded(126.7, price.doubleValue(), DEVIATION_PERCENTAGE);

        product.setBasePrice(BigDecimal.ZERO);
        price = priceService.compute("Dvouramenná 8", "Karlovo nám. 13, Praha", product);
        assertRounded(68.7, price.doubleValue(), DEVIATION_PERCENTAGE);

        product.setPricePerKm(BigDecimal.ZERO);
        price = priceService.compute("Dvouramenná 8", "Karlovo nám. 13, Praha", product);
        assertRounded(0D, price.doubleValue(), DEVIATION_PERCENTAGE);
    }

    /**
     * @param value
     * @param deviationPercentage from 0 to 1
     */
    private void assertRounded(Double expected, Double value, Double deviationPercentage) {
        Double deviation = Math.max(expected * deviationPercentage, 0.01);//in case expected is 0
        assertTrue("Price is not in expected range. Can be caused by different route found by gmaps.", expected - deviation < value && value < expected + deviation);
    }
}
