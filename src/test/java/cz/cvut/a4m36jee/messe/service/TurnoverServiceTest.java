package cz.cvut.a4m36jee.messe.service;

import cz.cvut.a4m36jee.messe.BaseArquillianTest;
import cz.cvut.a4m36jee.messe.model.Member;
import cz.cvut.a4m36jee.messe.model.Turnover;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(Arquillian.class)
public class TurnoverServiceTest extends BaseArquillianTest {
    @Inject
    TurnoverService turnoverService;

    @Inject
    MemberService memberService;

    @Test
    public void testGenerateTurnovers() {
        turnoverService.generateTurnovers("01.06.2016", "30.06.2016");
        try {
            Thread.sleep(1000);//time to complete batch job
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Member member = memberService.findByEmail("lionel@gmail.com");
        List<Turnover> turnoverList = turnoverService.findAllForUser(member);
        assertEquals(1, turnoverList.size());
        assertEquals(BigDecimal.valueOf(30.66), turnoverList.get(0).getProfit());
    }
}
